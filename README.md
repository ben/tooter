# tooter

irc tooter bot

deps:
* [Mastodon.py](https://github.com/halcy/Mastodon.py)

edit channels and botnick to test somewhere in irc

## install systemd user unit
1. edit paths to WorkingDirectory and ExecStart
1. `cp tooter.service ~/.config/systemd/user/`
1. `systemctl --user enable --now tooter`
